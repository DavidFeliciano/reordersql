package GroupInserts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/**
 * This code takes the input file entrada.sql to gather all inserts of the same type and join them into one, thus enabling a faster and more efficient database creation. 
 * It then writes everything to the output file 
 * @author David Feliciano Rivera Gómez
 */
public class mainmain {

	public static void main(String[] args) {
		
		// Configure path
		String pathIn = "src/ficheros/BossDB_edit2.sql";
		String pathOut = "src/ficheros/BossDB_edit2.sql";
		String pathOrder = "src/ficheros/orden.txt";
		
		// check if is the same file
		boolean equals = false;
		if (pathIn.equals(pathOut)) 
		{
			equals = true;
			pathOut = "temp.sql";
		} 
		
		// Group Inserts
		inALine(pathIn);
		ArrayList<String> ord = order(inserts("potato.sql"), pathOrder);
		String[][] content = new String[ord.size()][2];
		for (int i=0; i<ord.size(); i++) 
		{
			content[i][0] = ord.get(i);
			content[i][1] = grabContent(ord.get(i), "potato.sql");
		}
		toEmpty(pathOut);
		writeTable(pathIn, pathOut);
		System.out.println("It will be true if the process has finished "+ writeFile(content, pathOut));
		if (equals==true) transform(pathIn, pathOut);
		File f3 = new File("potato.sql");
		f3.delete();
		
	}
	
	/**
	 * This is the first function that runs, it's to put all the inserts individually on one line each, thus joining inserts from multiple lines and multiple inserts on a single line.
	 * @param pathIn The location of the source file for the inserts.
	 * @author David Feliciano Rivera Gómez
	 */
	private static void inALine(String pathIn) 
	{
		File f = new File(pathIn);
		File f2 = new File("potato.sql");

		try 
		{
			FileReader fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr); 
	        boolean primero = true;
	        while (br.ready()) 
	        {
    			FileWriter fw = new FileWriter(f2, true);
    			BufferedWriter bw = new BufferedWriter(fw); 
	        	String line = br.readLine();
	        	String[] thing = line.split(";");
	        	for (int i=0; i<thing.length; i++) 
        		{
		        	if (primero==true && (thing[i].contains("create") || thing[i].contains("insert") || thing[i].contains("use"))) 
		        	{
		        		bw.append(thing[i]);
		        		primero = false;
		        	}
		        	else if (primero==false) 
		        	{
		        		if (thing[i].contains("create") || thing[i].contains("insert") || thing[i].contains("use")) bw.append("; \n" + thing[i]);
		        		else bw.append(thing[i]);
		        	}
        		}
    			bw.flush();           
    			bw.close();	
    			fw.close();
	        }
	        br.close();
	        fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This function will only execute when pathIn and pathOut are the same. So, with a modified pathOut, everything will be created in the pathOut file. Therefore, 
	 * in this function, the pathIn file is deleted and then replaced with the modified pathOut file name.
	 * @param pathIn The location of the source file for the inserts.
	 * @param pathOut The location of the file where you want to place the inserts on a single line.
	 * @author David Feliciano Rivera Gómez
	 */
	private static void transform(String pathIn, String pathOut) {
		File f = new File(pathIn);
		File f2 = new File(pathOut);
		f.delete();
		f2.renameTo(f);
		
	}

	/**
	 * This function ensures that if the file salida.sql exists, the file is empty.
	 * @author David Feliciano Rivera Gómez
	 * @param pathOut The location of the file where you want to place the inserts on a single line.
	 */
	private static void toEmpty(String pathOut)
	{
		File f2 = new File(pathOut);
		FileWriter fw;
		try {
			fw = new FileWriter(f2, false);
			BufferedWriter bw = new BufferedWriter(fw); 
			bw.append("-- file modified by GroupInserts.sql, (https://gitlab.com/DavidFeliciano/reordersql)\n");
			bw.flush();           
			bw.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This function reads the file entrada.sql in order to read the tables and also add the creation of the tables if the file entrada.sql has them. It only reads the 
	 * tables if they are before the inserts; if there are any after, it won't work. It writes them to the file salida.sql until it reads an insert, 
	 * then stops the function.
	 * @author David Feliciano Rivera Gómez
	 * @param pathOut The location of the file where you want to place the inserts on a single line.
	 * @param pathIn The location of the source file for the inserts.
	 * @return Boolean that checks if the file has been written or not.
	 */
	private static boolean writeTable(String pathIn, String pathOut) 
	{
		
		try 
		{
			File f = new File(pathIn);
			File f2 = new File(pathOut);
			FileReader fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr); 
	        
	        boolean insert = false;
	        
	        while (br.ready()) 
	        {
	        	String line = br.readLine();
	        	if (line.contains("insert into")) insert = true;
	        	else if (line.contains("create")) insert = false;
	        	
	        	if (insert == false) 
	        	{

	    			FileWriter fw = new FileWriter(f2, true);
	    			BufferedWriter bw = new BufferedWriter(fw); 
	    			bw.write(line+"\n");
	    			bw.flush();           
	    			bw.close();	
	    			fw.close();
	        	}
	        }
	        br.close();
	        fr.close();
	        
		}
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.out.println("General write error");
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * This function writes to the file in append mode, allowing to write the inserts along with their values from the content of entrada.sql, and put everything 
	 * on a single line in salida.sql.
	 * @author David Feliciano Rivera Gómez
	 * @param pathOut The location of the file where you want to place the inserts on a single line.
	 * @param is a String[][] that will contain the content of the inserts. Specifically, position [0] will have the insert, and position [1] will contain the String 
	 * with all the values to insert.
	 * @return Boolean that checks if the file has been written or not.
	 */
	private static boolean writeFile(String[][] content, String pathOut) 
	{
		try 
		{
			File f = new File(pathOut);
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);  
			for (int i=0; i<content.length; i++) 
			{
				bw.write(content[i][0]+" values "+content[i][1]+";\n");
			}
			bw.flush();           
			bw.close();	
			fw.close();
			
			return true;
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.out.println("General write error");
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * This function reads the content of entrada.sql to be able to read the insert again, and in case it's the same insert as the header parameter, it will add the 
	 * content of the insert, removing any semicolons if they exist and adding a comma, so that the grouped insert works correctly.
	 * @author David Feliciano Rivera Gómez
	 * @param insert A String that is specifically an insert from the ArrayList after going through the sort function (order).
	 * @param pathIn The location of the source file for the inserts.
	 * @return A String with the content of entrada.sql where the insert is equal to the String insert
	 */
	private static String grabContent(String insert, String pathIn) 
	{         
        String line = "";
		try 
		{
			File f = new File(pathIn);
			FileReader fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr); 
	        
	        while (br.ready()) 
	        {
	        	String linea = br.readLine();
	        	if (linea.toLowerCase().contains("values"))
	        	{
	        		String[] ne = null;
		        	if (linea.contains("values")) 
	        		{
			        	ne = linea.split("values");
	        		}
	        		if (linea.contains("VALUES")) 
	        		{
			        	ne = linea.split("VALUES");
	        		}
		        	if (ne!=null) 
		        	{
			        	String cab = ne[0];
			        	if (cab.equals(insert) && ne[1].contains(";")) 
			        	{
			        		String[] fi = ne[1].split(";");
			        		if (line.length()==0) line += fi[0];
			        		else line += ", " + fi[0];
			        	}
		        	}
	        	}
	        }
	        br.close();
	        fr.close();
		}
		 catch (IOException e) 
		 {
	         System.out.println("General write error");
	         e.printStackTrace();
	     }
		return line;
	}
	

	/**
	 * This function will take the ArrayList where the Inserts are contained to sort them according to the user's preferences as described in the orden.txt file. 
	 * (It's important that the file contains ' (table) ' the spaces will help differentiate tables from connection tables that also contain the same word).
	 * @author David Feliciano Rivera Gómez
	 * @param arr ArrayList of String that will contain the elements to be sorted.
	 * @param pathOrder The location of the file order.txt to establish the order.
	 * @return ArrayList containing all the 'insert into' statements sorted according to preference.
	 */
	private static ArrayList<String> order(ArrayList<String> arr, String pathOrder) 
	{
		File f = new File(pathOrder);
		if (f.exists()==true) 
		{
			try 
			{
				FileReader fr = new FileReader(f);
		        BufferedReader br = new BufferedReader(fr);          
		        ArrayList<String> order = new ArrayList<String>();
		        
		        while (br.ready()) 
		        {
		        	String line = br.readLine();
		        	for (int i=0; i<arr.size(); i++) 
		        	{
		        		if (arr.get(i).contains(line)) order.add(arr.get(i));
		        	}
		        	
		        }
		        
		        if (order.size()!=arr.size()) 
		        {
		        	for (int i=0; i<arr.size(); i++) 
		        	{
		        		if (!(order.contains(arr.get(i))))
		        		{
		        			order.add(arr.get(i));
		        		}
		        	}
		        }
		        br.close();
		        fr.close();
		        return order;
		     }
			 catch (IOException e) 
			 {
		         System.out.println("General write error");
		         e.printStackTrace();
		     }
		}
		return arr;
	}
	
	/**
	 * This function will read the entrada.sql file and fetch the inserts to put them into an ArrayList for identification purposes. Additionally, it checks that they are 
	 * not already in the ArrayList to ensure that they are not repeated.
	 * @author David Feliciano Rivera Gómez
	 * @param path The location of the source file for the inserts.
	 * @return ArrayList containing all the 'insert into' statements from the entrada.sql file.
	 */
	private static ArrayList<String> inserts(String path) 
	{
		try 
		{
			File f = new File(path);
			FileReader fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr);          
	        ArrayList<String> ins = new ArrayList<String>();
	        while (br.ready()) 
	        {
	        	String line = br.readLine();
	        	String[] thing = line.split(";");
	        	if (line.toLowerCase().contains("values"))
	        	{
	        		String[] ne = null;
	        		if (line.contains("values")) 
	        		{
			        	ne = line.split("values");
	        		}
	        		else if (line.contains("VALUES")) 
	        		{
			        	ne = line.split("VALUES");
	        		}
	        		if (!(ins.contains(ne[0]))) 
	        		{
	        			ins.add(ne[0]);
	        		}
	        	}
	        }
	        br.close();
	        fr.close();
	        return ins;
	     } 
		 catch (FileNotFoundException e) 
		 {
	         System.out.println("File not found");
	         e.printStackTrace();
	     } 
		 catch (IOException e) 
		 {
	         System.out.println("General write error");
	         e.printStackTrace();
	     }
		return null;
	}
}
