<h1> GroupInserts.SQL </h1>

GroupInserts.SQL is a code written in JAVA11 designed to gather all the inserts from a SQL file to merge them and make the code faster and more efficient.

You can use it and modify it to your liking. Thank you for using my code.

<h3>Usage Tutorial</h3>

<h4>1. Choose file</h4>

To make the program work, you need a file where you want to gather the inserts, for this, you need the SQL file. To make the code work, the file must be named and located where you specify in the code on line **_23_**.

Furthermore, it's important that each insert or create statement is separated by a semicolon **;**, whether the insert spans multiple lines or multiple inserts occupy a single line, the semicolon will separate each insert to distinguish them from each other.

<h4>2. Sort the inserts</h4>

The problem with the program is that it cannot know how the table is structured, therefore in some cases it is necessary to modify the order in which the inserts are written in the new file because in some cases you cannot make an insert because you would need other inserts previously to be able to make that one.

For this reason, on line _**25**_ of the code, you can place the location of the order file, we'll call it _**order.txt**_. In this file, you can write the name of the tables in the order you want, when using the **order** function then the ArrayList with the inserts will be sorted according to the contents of this file.

For _**order.txt**_ to work correctly, the content of the file must have the following format: _**(space)(table name)(space)**_. This is to avoid the order being confused with tables that contain the same word as the table name. For example, in the example file there are tables **Productos** and **JefeProductos**, if you don't put spaces, both **Productos** and **JefeProductos** will be ordered at the same time, even before the table **Jefes** is put, which is necessary for **JefeProductos** to work.

Additionally, it is necessary that there is only one table per line.

Important: this file is optional, if the file does not exist, nothing happens and the code will simply order according to how the inserts appear in the input file. Also, if there is any table misspelled, it will simply ignore it and skip it.

<h4>3. Output file</h4>

The location of the output file, where the inserts will be written on a single line, would be chosen on line **_24_** of the code. If it's the same as the original file, it will be modified; otherwise, a new file will be created.

If the file does not exist, it will create it, if it exists, it will delete the content before writing the new content.

If the input file contains the creation of tables in addition to the inserts, the code will write it in the output file but only if they are before all the inserts, if there is any insert before the creation of a table, this table will not be written in the output file.

After writing the table, the inserts will be written in a single line, when finished it will display a message, the message is '_true should be true_', if it is '_false should be true_' it implies that something has failed and therefore the inserts have not been written to the output file.

<h4>4. Execute</h4>

To execute, you have to run the code in the script _**mainmain.java**_ located at _GroupInsertsSQL/src/GroupInserts/mainmain.java_. When running it, it should perform the entire process to gather the inserts and create them in a new file.